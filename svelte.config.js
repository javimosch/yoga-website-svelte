import adapter from '@sveltejs/adapter-static';
import importAssets from 'svelte-preprocess-import-assets'
import sveltePreprocess from 'svelte-preprocess';

/** @type {import('@sveltejs/kit').Config} */
const config = {
	preprocess: [importAssets(),sveltePreprocess({
		scss:{
			prependData: `@import 'src/lib/assets/main.scss';`
		}
	})],
	kit: {
		// hydrate the <div id="svelte"> element in src/app.html
		target: '#svelte',
		adapter: adapter({
			// default options are shown
			pages: 'build',
			assets: 'build',
			fallback: null
		}),
	}
};

export default config;
