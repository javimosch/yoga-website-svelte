import { derived, writable } from 'svelte/store';
import frLangs from '/src/assets/fr.json';
import enLangs from '/src/assets/en.json';

let fallbackLanguage = 'fr'
let currentLanguage = 'fr';
const availableLocales = {
	fr: frLangs,
	en: enLangs
};

export const locales = writable(availableLocales[currentLanguage]);

export function switchLanguage(langName) {
	currentLanguage = langName;
	locales.set(availableLocales[currentLanguage]);
}

export const t = derived(
	locales,
	$locales => (path)=> $locales[path] || availableLocales[fallbackLanguage][path] || path || ""
);

